package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployeeDao;
import com.model.Employee;

@RestController
public class EmployeeController {
	@Autowired
	EmployeeDao empDao;
	
	@RequestMapping("displayEmp")
	public Employee displayEmp(){
		Employee employee = new Employee();
		employee.setEmpId(1001);
		employee.setEmpName("SACHIN");
		employee.setSalary(9999.99);
		employee.setEmailId("SACHIN@GMAIL.COM");
		employee.setPassword("PASSWORD");
		return employee;
	}
	@GetMapping("displayAllEmp")
	public List<Employee> displayAllEmp(){
		return empDao.getAllEmployees();
	}
	
	@PostMapping("registerEmp")
	public String registerEmp(@RequestBody Employee employee){
		empDao.register(employee);
		return "Registrations Successful";
	}
	
	@GetMapping("displayEmpById/{id}") 
	public Employee displayEmpById(@PathVariable("id") int id){
		Employee e1 = new Employee();
		e1.setEmpId(1001);
		e1.setEmpName("SACHIN");
		e1.setSalary(9999.99);
		e1.setEmailId("SACHIN@GMAIL.COM");
		e1.setPassword("PASSWORD");

		Employee e2 = new Employee();
		e2.setEmpId(1002);
		e2.setEmpName("DHONI");
		e2.setSalary(8888.88);
		e2.setEmailId("DHONI@GMAIL.COM");
		e2.setPassword("PASSWORD");


		Employee e3 = new Employee();
		e3.setEmpId(1001);
		e3.setEmpName("ROHITH");
		e3.setSalary(7777.99);
		e3.setEmailId("ROHITH@GMAIL.COM");
		e3.setPassword("PASSWORD");

		List <Employee> empList = new ArrayList<Employee>();
		empList.add(e1);
		empList.add(e2);
		empList.add(e3);

		for(Employee emp : empList){
			if(emp.getEmpId() == id)
				return emp;
		}
		return null;
	}
	
	@DeleteMapping("deleteEmp/{empId}")
	public void deleteEmp(@PathVariable("empId") int empId){
		empDao.deleteEmp(empId);
	}
	@PutMapping("updateEmp")
	public String updateEmp(@RequestBody Employee employee){
		empDao.updateEmp(employee);
		return "Registrations Successful";
	}
}
